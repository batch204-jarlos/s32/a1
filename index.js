const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {

	if (request.url == '/' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to booking system")
	} else if (request.url == '/profile' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcomet to your profile")
	} else if (request.url == '/courses' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Here's our courses available")
	} else if (request.url == '/addcourse' && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Add a course to our resources")
	} else if (request.url == '/updatecourse' && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Update a course to our resources")
	} else if (request.url == '/archivecourses' && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Archive courses to our resources")
	}

});

server.listen(port);
console.log(`Server is successfully running`)